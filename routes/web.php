<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return response()->json([
        'response' => 'Funcionando...'
    ]);
});

// Route::p('/login',function(Request $request){
//     // return $request->all();
//     $newProduct = new Product;
//     $newProduct -> description = $request -> input('description');
//     $newProduct -> price = $request -> input('precio');

//     $newProduct->save();

//     return redirect()->route('products.index')->with('info',"Se guardó con éxito");

// })->name('products.store');
